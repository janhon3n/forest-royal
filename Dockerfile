FROM node:10
RUN mkdir /app
WORKDIR /app
COPY package.json .
COPY package-lock.json .

ENV REACT_APP_SERVER_ADDRESS https://royalserver.janho.fi

RUN npm install && npm install --global serve
COPY . .
RUN npm run build
CMD serve -p 80 ./build/
