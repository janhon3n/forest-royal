import React, { Component } from 'react';
import Menu from './Menu/Menu'
import GameWrapper from './Game/GameWrapper'

class App extends Component {

  constructor(props) {
    super(props)

    this.joinGame = this.joinGame.bind(this)
    
    this.state = {
      gameCode: null,
    }
  }

  joinGame(gameCode) {
    this.setState({gameCode})
  }

  render() {
    return (
      <div className="App">
        {
          this.state.gameCode ? 
          <GameWrapper gameCode={this.state.gameCode} onGameExit={(reason) => {
            this.setState({gameCode: null})
            if(reason) {
              alert(reason)
            }
          }} />
          :
          <Menu onJoinGame={this.joinGame} />
        }
      </div>
    );
  }
}

export default App;
