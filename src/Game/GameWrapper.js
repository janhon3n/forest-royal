import React, { Component } from 'react'
import PlayGame from './PlayGame'
import WaitingLobby from './WaitingLobby'
import io from 'socket.io-client'

class GameWrapper extends Component {
  constructor(props){
    super(props)

    this.connectToGameServer = this.connectToGameServer.bind(this)
    this.updatePosition = this.updatePosition.bind(this)
    this.updateGameState = this.updateGameState.bind(this)

    this.state = {
      gameState: null,
    }    
  }

  async componentDidMount() {
    try {
      await this.connectToGameServer()
    } catch(err) {
      return this.props.onGameExit()
    }

    console.log(this.socket)
    this.socket.on('update_state', this.updateGameState)
    console.log(this.socket)

    window.onbeforeunload = function(){
      return 'Are you sure you want to leave?';
    }
    this.positionUpdatingInterval = setInterval(this.updatePosition, 5000)
    this.updatePosition()
  }

  async connectToGameServer() {
    return new Promise((resolve, reject) => {
      this.socket = io(window.serverAddress + '/' + this.props.gameCode)
      this.socket.on('connect', resolve)
      this.socket.on('connect_error', reject)
    })
  }

  async updatePosition() {
    let position = await window.map.getPosition()
    this.socket.emit('set_position', {
      latitude: position.latitude, longitude: position.longitude
    })
    window.map.updatePlayerMarker(position)
  }

  componentWillUnmount() {
    this.socket.disconnect()
    window.onbeforeunload = undefined
    clearInterval(this.positionUpdatingInterval)
  }


  updateGameState(gameState) {
    console.log(gameState)
    let player = gameState.players.find(p => {
      return p.id === this.socket.id
    })
    // If player not found in game
    if (!player) {
      // Exit from game
      this.props.onGameExit("Player got removed from server")
    }

    window.map.updateCircle(gameState.circle.center, gameState.circle.radius)

    this.setState(prevState => {
      let newMessages = []
      if(prevState.gameState)
        newMessages = prevState.gameState.messages.slice()
      
      if (gameState.message) newMessages.push(gameState.message)
      gameState.messages = newMessages
      return {
        gameState,
      }
    })
  }

  render () {
    let {onGameExit, gameCode} = this.props
    let {gameState} = this.state

    if(!gameState) {
      return <div>Connecting to game...</div>
    }

    let player = gameState.players.find(p => p.id === this.socket.id)
    if(!player.alive) {
      return (
        <div>You are dead
          <button onClick={() => onGameExit()}>Leave</button>
        </div>
        )
    }

    if(gameState.hasStarted) {
      return <PlayGame
        socket={this.socket}
        gameState={gameState}
        player={player}
        onGameExit={onGameExit} />
    } else {
      return <WaitingLobby
        socket={this.socket}
        gameState={gameState}
        gameCode={gameCode}
        player={player} />
    }
  }
}

export default GameWrapper