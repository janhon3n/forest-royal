import React from 'react'

export default class Messages extends React.Component{


  render() {
    let messages = this.props.children.slice()
    messages.reverse()
    return (
      <ul style={{fontSize: '0.9rem', overflowY: 'auto', maxHeight: '25vh', width: '100vw', padding: '10px', backgroundColor: 'black'}}>
        {
          this.props.expanded ? 
            messages.map(msg => {
            return <li>{msg}</li>
          }) :
          <li>{messages[0]}</li>
        }
      </ul>
    )
  }
}