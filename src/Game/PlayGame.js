import React, { Component } from 'react'
import Messages from './Messages'
import Timer from './Timer'
import PlayerList from './PlayerList';

class GameWrapper extends Component {
  constructor(props){
    super(props)

    this.reportKilled = this.reportKilled.bind(this)

    this.state = {
      tab: 'play'
    }
  }

  componentDidUpdate(props) {
    if (props.gameState.hasEnded) this.props.onGameExit("Game has ended")
  }

  reportKilled() {
    this.props.socket.emit('set_killed')
  }

  render () {
    let {gameState} = this.props

    let timerAndButton = (
        <div style={{flexDirection: 'row', padding:'5px'}}>
          <Timer
            startTime={gameState.phase.startTime}
            duration={gameState.phase.duration} />
          <button style={{marginLeft: '10px'}} onClick={this.reportKilled}>I got killed</button>
      </div>
    )

    return <div>
      <button onClick={() => this.setState({tab: 'play'})}>Map</button>
      <button onClick={() => this.setState({tab: 'stats'})}>Stats</button>
      {this.state.tab === 'stats' ? 
        <div style={{maxHeight: '70vh'}}>
          <Messages expanded={true}>{gameState.messages}</Messages>
          <PlayerList players={gameState.players} showReady={false} />
          <label style={{textAlign: 'center'}}>Kills</label>
          <span>0</span>
          {timerAndButton}
        </div>
        :
        <div>
          <Messages>{gameState.messages}</Messages>
          {timerAndButton}
        </div>
      }
    </div>
  }
}

export default GameWrapper