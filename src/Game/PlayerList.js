import React, { Component } from 'react'

class PlayerList extends Component {
  render () {
    return (
      <div style={{padding: '5px'}}>
        <label style={{textAlign: 'center'}}>Players</label>
        <table style={{fontSize: '0.9rem'}}>
          {this.props.players.map(p => {
            return (
            <tr><td>{p.tag}</td>
              {this.props.showReady ? <td style={{width: '80px'}}>{p.ready ? "Ready" : " - "}</td> : null}
            </tr>
            )
          })}
        </table>
      </div>
    )
  }
}

export default PlayerList