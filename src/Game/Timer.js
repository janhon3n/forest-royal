import React, { Component } from 'react'

class Timer extends Component {

  componentDidMount() {
    this.interval = setInterval(() => {
      this.forceUpdate()
    }, 1000)
  }
  componentWillUnmount() {
    clearInterval(this.interval)
  }

  render () {
    let {startTime, duration} = this.props

    let timeLeft = startTime + duration * 1000 * 60 - (new Date().getTime())
    let minutes = Math.floor(timeLeft / (1000 * 60))
    let seconds = Math.floor((timeLeft - minutes * 60000) / 1000)

    if(seconds < 10) {
      seconds = "0" + seconds
    }
    
    return (
      <div style={{width: '140px'}}>
        {timeLeft < 0 ? '0:00' : minutes + " : " + seconds}
      </div>
    )
  }
}

export default Timer