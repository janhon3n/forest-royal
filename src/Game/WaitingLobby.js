import React, { Component } from 'react'
import Messages from './Messages'
import PlayerList from './PlayerList'

class WaitingLobby extends Component {
  render () {
    let {gameState, gameCode, player, socket} = this.props
    return (
      <div>
        <Messages>{gameState.messages}</Messages>
        <div style={{padding: '10px'}}>
          <label style={{textAlign: 'center'}}>Game code</label>
          {gameCode}
          <PlayerList players={gameState.players} showReady={true} />

          { !player.ready ? 
            <button onClick={() => {
              socket.emit('set_ready', true)
            }}>Ready</button>
            : 
            <button onClick={() => {
              socket.emit('set_ready', false)
            }}>Cancel ready</button>
          }
        </div>
      </div>
    )
  }
}

export default WaitingLobby