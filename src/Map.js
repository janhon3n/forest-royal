var map_styles = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#242f3e"
      }
    ]
  },
  {
    "elementType": "labels",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#746855"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#242f3e"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "administrative.neighborhood",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "landscape",
    "elementType": "geometry",
    "stylers": [
      {
        "lightness": 100
      }
    ]
  },
  {
    "featureType": "landscape.natural",
    "stylers": [
      {
        "color": "#c8d1df"
      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "landscape.natural.terrain",
    "stylers": [
      {
        "lightness": 55
      },
      {
        "visibility": "on"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#263c3f"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#6b9a76"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#38414e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#212a37"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9ca5b3"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#746855"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#1f2835"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#f3d19c"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#2f3948"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#17263c"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#515c6d"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#17263c"
      }
    ]
  }
]


export default class Map {
  constructor(element, playerPosition) {
    let pos = this.toGoogleCoords(playerPosition)
    this.map = new window.google.maps.Map(element, {
      center: pos,
      zoom: 12,
      fullscreenControl: false,
      mapTypeControl: false,
      streetViewControl: false,
      styles: map_styles,
      backgroundColor: 'none',
    });

    this.playerMarker = new window.google.maps.Marker({
      position: pos,
      map: this.map,
    })

    this.circle = new window.google.maps.Circle({
      strokeColor: 'white',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#ffdd00',
      fillOpacity: 0.35,
      map: null,
      center: pos,
      radius: 0,
    });
  }

  toGoogleCoords(coords) {
    return {
      lat: coords.latitude,
      lng: coords.longitude
    }
  }
  
  fromGoogleCoords(coords) {
    return {
      latitude: coords.lat(),
      longitude: coords.lng()
    }
  }

  updatePlayerMarker(position) {
    this.playerMarker.setPosition(this.toGoogleCoords(position))
  }

  updateEnemyMarkers(positions) {
    for (let m in this.enemyMarkers) {
      m.setMap(null)
    }
    this.enemyMarkers = []
    for (let p in positions) {
      this.enemyMarkers.push(
        new window.google.maps.Marker({
          position: this.toGoogleCoords(p),
          map: this.map,
        })
      )
    }
  }

  updateCircle(position, radius) {
    if(position)
      this.circle.setCenter(this.toGoogleCoords(position))
    this.circle.setRadius(radius)
    this.circle.setMap(this.map)
  }

  enableCircleSelectionMode() {
    this.circle.setCenter(this.map.getCenter())
    this.circle.setRadius(1000)
    this.circle.setMap(this.map)
    this.circle.setDraggable(true)
  }

  disableCircleSelectionMode() {
    this.circle.setDraggable(false)
  }

  getCircleSelection() {
    return {
      center: this.fromGoogleCoords(this.circle.getCenter()),
      radius: this.circle.getRadius()
    }
  }

  getPosition() {
    return new Promise((resolve, reject) => {
      window.navigator.geolocation.getCurrentPosition((pos) => {
        resolve(pos.coords)
      }, reject)
    })
  }
}