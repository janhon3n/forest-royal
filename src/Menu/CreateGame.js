import React, { Component } from 'react'
import io from 'socket.io-client'

class CreateGame extends Component {
 
  constructor(props) {
    super(props)

    this.createGame = this.createGame.bind(this)

    this.state = {
      duration: 10,
      radius: 1000,
    }
  }

  componentDidMount() {
    window.map.enableCircleSelectionMode()
  }

  createGame() {
    let gameManager = io(window.serverAddress + '/game_manager')
    gameManager.on('connect', () => {
      gameManager.emit('create_game', {
        phaseDuration: this.state.duration,
        circle: window.map.getCircleSelection(),
      }, (gameCode) => {
        window.map.disableCircleSelectionMode()
        gameManager.disconnect()
        this.props.onJoinGame(gameCode)
      })
    })
  }

  render () {
    let {duration, radius} = this.state

    return (
      <div>
        <label>Phase Duration (min)</label>
        <input onChange={(e) => {
          this.setState({duration: e.target.value})
        }} type="number" min="1" max="20" value={duration} />
        <label>Map radius</label>
        <input onChange={(e) => {
          let val = Number.parseInt(e.target.value)
          this.setState({radius: val})
          window.map.updateCircle(null, val)
        }} type="range" min="30" max="2000" value={radius} />
        <button onClick={this.createGame}>Create</button>
      </div>
    )
  }
}

export default CreateGame