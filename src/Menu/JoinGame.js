import React from 'react'

export default class JoinGame extends React.Component {
  
  constructor(props) {
    super(props)
    this.state = {
      gameCode: ""
    }
  }

  setGameCode(value) {
    let numerical = Number.parseInt(value)
    if (value === "" || (
      Number.isInteger(numerical) && numerical > 0
    )) {
      this.setState({gameCode: value})
    }
  }

  render() {
    let {gameCode} = this.state
    let {onJoinGame} = this.props
    return (
      <div>
        <label>Game code</label>
        <input
          type="text" 
          value={gameCode}
          onChange={(e) => this.setGameCode(e.target.value)} />
        <button onClick={() => onJoinGame(gameCode)}>Join</button>
      </div>
    )
  }
}