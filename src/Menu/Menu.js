import React, { Component } from 'react'
import CreateGame from './CreateGame';
import JoinGame from './JoinGame'

class Menu extends Component {

  constructor(props) {
    super(props)

    this.state = {
      phase: null,
    }
  }

  validGameCode(str) {
    var n = Math.floor(Number(str));
    return n !== Infinity && String(n) === str && n >= 0;
}

  render () {
    let {onJoinGame} = this.props
    let {phase} = this.state

    if (phase === 'join') {
      return <JoinGame onJoinGame={onJoinGame} />
    }

    if(phase === 'create') {
      return <CreateGame onJoinGame={onJoinGame} />
    }

    return (
      <div style={{ padding: '10px' }}>
        <button onClick={() => this.setState({phase: 'join'})}>
          Join game
        </button>
        <button onClick={() => this.setState({phase: 'create'})}>
          Create game
        </button>
      </div>
    )
  }
}

export default Menu