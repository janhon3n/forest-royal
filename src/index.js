import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Map from './Map'

window.serverAddress = process.env.REACT_APP_SERVER_ADDRESS ? process.env.REACT_APP_SERVER_ADDRESS : 'http://localhost:3001'

let mapApiKey = process.env.REACT_APP_MAP_API_KEY ? process.env.REACT_APP_MAP_API_KEY : 'AIzaSyAaFgHAHHLwCZbPsfJH7wpg7ACc8ZnvY8I'
let mapScript= document.createElement('script')
mapScript.setAttribute('async', true)
mapScript.setAttribute('deref', true)
mapScript.setAttribute('src', 'https://maps.googleapis.com/maps/api/js?key='+mapApiKey+'&callback=initMap')
document.head.appendChild(mapScript);

ReactDOM.render(<App />, document.getElementById('root'))

window.initMap = () => {
  window.navigator.geolocation.getCurrentPosition(pos => {
    window.map = new Map(document.getElementById('map'), pos.coords)
  })
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
